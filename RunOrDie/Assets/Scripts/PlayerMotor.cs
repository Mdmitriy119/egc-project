﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotor : MonoBehaviour
{
    private CharacterController controller;
    private Vector3 moveVector;
    public Animator animator;
    public AudioSource audioSrc;
    private float speed = 8.0f;
    private float verticalVelocity = 0.0f;
    private float gravity = 6.0f;
    private float jumpForce = 6.0f;
    private float animationDuration = 3.0f;
    private float startTime;

    private bool isDead = false;
     
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        startTime = Time.time;

    }

    // Update is called once per frame
    void Update()
    {
        if (isDead) return;
        if (verticalVelocity < -10) Death();
        if (Time.time -startTime < animationDuration)
        {
            controller.Move(Vector3.forward * speed * Time.deltaTime);
            return;
        }

        moveVector = Vector3.zero;

        if (controller.isGrounded)
        {
            verticalVelocity = -0.5f;
           if (Input.GetKeyDown(KeyCode.Space))
            {
                verticalVelocity = jumpForce;
                animator.SetTrigger("Jump");
            }
        }
        else
        {
            verticalVelocity -= gravity * Time.deltaTime;
        }

        
        //X - Left and Right
        moveVector.x = Input.GetAxisRaw("Horizontal")*speed;

        //Y - Up and Down
        moveVector.y = verticalVelocity;

        //Z - Forward and Backward
        moveVector.z = speed;

        controller.Move(moveVector*Time.deltaTime); 

    }
    public void SetSpeed(float modifier)
    {
        speed = 6.0f + modifier;
    }

    //It is being called every time our capsule hits something
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if(hit.point.z > transform.position.z + controller.radius)
        {
            Death();
        }
    }
    
    private void Death()
    {
        isDead = true;
        audioSrc.Stop();
        GetComponent<Score>().OnDeath();
    }
}
