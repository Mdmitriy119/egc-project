﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class VolumeValueChangedGame : MonoBehaviour
{
    // Start is called before the first frame update
    private AudioSource AudioSrc;

    private float AudioVolume = 1f;

    void Start()
    {
        AudioSrc = GetComponent<AudioSource>();
    }

    void Update()
    {
        AudioSrc.volume = AudioVolume;
    }

    public void SetVolume(float vol)
    {
        AudioVolume = 0;
        
    }
}
